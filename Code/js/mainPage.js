// Code for the main app page (Regions List).

// Establish the array which acts as a data source for the list
var array = JSON.parse(localStorage.getItem("monash.eng1003.fencingApp"))


{   
   // For loop to display Regions saved in local Storage by the user in the mainPage
   for (var i = 0; i < array.length; i++)
  {
   
     var li = document.createElement("li");
     li.className = "mdl-list__item mdl-list__item--two-line";
   
     var a = document.createElement('a');
     //Setting an attribute of onClick for the Region Name so that the user can View his region
     a.setAttribute("onClick","viewRegion("+i+")");
     a.setAttribute("style","color:black;cursor:pointer");
     
     var regionName = document.createTextNode(array[i]._nickname);
   
     a.appendChild(regionName);
     a.style.fontFamily = 'Trebuchet MS';
     a.style.color = "white";
     a.style.fontSize = "24px";
     li.appendChild(a);
     
     var othertext = document.createElement('br')
     li.appendChild(othertext)
     var othertext = document.createElement('br')
     li.appendChild(othertext)
     var othertext = document.createElement('br')
     li.appendChild(othertext)
     othertext = document.createTextNode('Date Created:'+array[i]._Date )
     // Some styles added to display the Regions
     li.style.fontFamily = "Arial, Helvetica, sans-serif";
     li.style.fontSize = "16px"
     li.appendChild(othertext);
     othertext = document.createTextNode( ' Time: ' +   array[i]._Time )
     li.appendChild(othertext);
     var othertext = document.createElement('br')
     li.appendChild(othertext)

   
     document.getElementById("myUl").appendChild(li);
     
   }
   
          
}

// Function which starts the process of viewing a saved Region
function viewRegion(regionIndex)
{
    // Save the desired region to local storage so it can be accessed from view region page.
    localStorage.setItem(STORAGE_KEY + "-selectedRegion", regionIndex); 
    // ... and load the view region page.
    location.href = 'viewRegion.html';
}
