// Shared code needed by the code of all three pages.

// Reference for calculation values
let calculationDialogRef = document.getElementById("calculationDialog");
// reference to the dialog title html element
let dialogTitleRef = document.getElementById("dialogTitle");

var defaultMaxDistance = 4;                        // default Maximum distance between fence posts
var STORAGE_KEY = "monash.eng1003.fencingApp";     // Prefix for Storing Regions in the local Storage
var geod = GeographicLib.Geodesic.WGS84;           // Link for the access of GeographicLib which is required for fence post calculations

// Array of saved Region objects.
var savedRegions = [];

// Retrieving the Maximum distance between fence posts stored in local Storage
var MaxDistance = localStorage.getItem(STORAGE_KEY + "-fencepostdistance");

// Checking if MaxDistance from local Storage is null if so equate MaxDistance to the default distance
if(MaxDistance === null)
    {
        MaxDistance = defaultMaxDistance;
    }


// The Class named Region is declared below
class Region 
{
  constructor(nickname,Date,Time,registeredLocation)
  {
    this._nickname = nickname;
    this._Date = Date;
    this._Time = Time;
    this._cornerLocations = [];
    this._registeredLocation = registeredLocation;
 
  }
  getDate()
  {
    return this._Date
  }
  addlocation(cornerlocation)
  {
     this._cornerLocations.push(cornerlocation)
  }
  getcornerlocation()
  {
    return this._cornerLocations
  }
  
  // Function which gets called by a region Instance when the user wants to calculate the area of a region
  calculateArea(coordinates)
  {
    // The parameter coordinates is an array consisting of cornerlocations of the polygon
    var p = geod.Polygon(false), i, coordinates
  
    for (i = 0; i < coordinates.length; ++i)    
      p.AddPoint(coordinates[i][1], coordinates[i][0]);
    p = p.Compute(false, true);
    
    // Code to display the area calculated in a dialog Box to the user
    dialogTitleRef.textContent = " Region Area " ;
    var areainfo =  "Area is " + p.area.toFixed(3) + " m^2.";
    calculationDialogRef.innerHTML = areainfo;
    showDialog()
    
  }
    
   // Function which gets called by a region Instance when the user wants to calculate the perimeter of a region
   calculatePerimeter(coordinates)
  {
    // The parameter coordinates is an array consisting of cornerlocations of the polygon
    var p = geod.Polygon(false), i, coordinates
  
    for (i = 0; i < coordinates.length; ++i)    
      p.AddPoint(coordinates[i][1], coordinates[i][0]);
    p = p.Compute(false, true);
    
    // Code to display the perimeter calculated in a dialog Textbox to the user
    dialogTitleRef.textContent = " Region Perimeter " ;
    var perimeterinfo =  "Perimeter is " + p.perimeter.toFixed(3) + "m";
    calculationDialogRef.innerHTML = perimeterinfo;
    showDialog()      
  }
    
    fence(coordinates)
   { 
        // The parameter coordinates is an array consisting of cornerlocations of the polygon
       
        //Initialzing some variables to store Fence post information;
       
        var fencePostsPerSide = [];        // Stores the total number of fences required for each side of a Region
        var numberOfFencePosts = 0;         // Stores the total number of fences required to cover the entire Region
        var distancePerSide = 0;            // Temporaryily stores the distance per side of a region
        var directions = [];                // Stores the directions in which the fence posts should be plotted on the region
        
        // For loop to calculate the distance per side of the region,the number of fence posts required per side,
        // the total number of fence post required and finally the directions
       
        for (var i =0; i < coordinates.length-1;i++)
        {
           // Syntax to calculate the distance from one point to another
           // r.s12 gives the distance
           // r.azi1 gives the direction
           var r = geod.Inverse(coordinates[i][1],coordinates[i][0],coordinates[i+1][1],coordinates[i+1][0])
           distancePerSide = r.s12;
           numberOfFencePosts += Math.floor(distancePerSide/MaxDistance);
           fencePostsPerSide.push(Math.floor(distancePerSide/MaxDistance));
           directions.push(r.azi1);
            
        }
        
    
        // Since the for loop above doesn't calculate the properties between the last point and first point we calculate them separately
        var r = geod.Inverse(coordinates[coordinates.length-1][1],coordinates[coordinates.length-1][0],coordinates[0][1],coordinates[0][0]);
        distancePerSide = r.s12;
        numberOfFencePosts += Math.floor(distancePerSide/ MaxDistance);
        fencePostsPerSide.push(Math.floor(distancePerSide/ MaxDistance));
        directions.push(r.azi1);
        
        // number of fence Posts also include the corners
        numberOfFencePosts += coordinates.length;

        // Displaying the total Number of fence required to the user through a textbox
        document.getElementById("textbox").innerHTML = "Number of Fence Posts Required : " +  numberOfFencePosts;
        
          
        var boundaryFencePosts = [];

        // For loop to calculate the fence post locations
        for(let i = 0; i < coordinates.length; i++)
           {
             for( let j = 0 ;j < fencePostsPerSide[i] ; j++ )
             {
                var distperPost = MaxDistance * (j + 1)
                // for loop calculates the coordinates of the fence posts to be plotted according to the direction per side
                var r = geod.Direct(coordinates[i][1],coordinates[i][0],directions[i],distperPost);
                boundaryFencePosts.push([r.lon2,r.lat2]);
             }
           }
       
        // returning the fence post coordinates so that markers can be plotted around the region
        return boundaryFencePosts;
     }
        
}


// below the declaration of the Class RegionList which stores the instances of the Region class
class RegionList
{
 constructor()
 {
    this._regionInstances = [];
    this.countofRegions = 0;
 }
 getregionInstances()
 {
   return this._regionInstances
 }
 addregion(regionInstance)
 {
   savedRegions.push(regionInstance)
 }
 removeregion(regionInstance)
 {
   this.regionInstance.pop(this._regionInstance.find(regionInstance))
 }

 getNumberOfRegions()
 {
   return this._regionInstances.length
 }
}

// Below the js code for the Settings Page

 var customMaxDistance;
{
  // Checking if there is an customMaxDistance in local Storagw
  customMaxDistance = localStorage.getItem(STORAGE_KEY + "-fencepostdistance");
    
   // If not setting the currentMaxDistance to defaultMaxDistance and then disabling the reset button
   if (customMaxDistance === null)
  {
    document.getElementById("currentMaxDistance").innerText = defaultMaxDistance + " metres";
    document.getElementById("resetButton").setAttribute("disabled","disabled");
  } 
   // If yes display the currentMaxDistance as customMaxDistance and the make the default reset button enabled
   else
  {
    document.getElementById("currentMaxDistance").innerText = customMaxDistance + " metres";
    document.getElementById("resetButton").removeAttribute("disabled");
  }

}

 // Function which runs when the user wants to change the Maximum Distance
 function changeDistance()
{
  var inValueRef = document.getElementById("customMaximumDistance")
  customMaxDistance = Number(inValueRef.value);
   
  // After the user confirms the change currentMaxDistance is displayed as customMaxDistance
  document.getElementById("currentMaxDistance").innerText = customMaxDistance + " metres";
  // Storing the customMaxDistance specified by the user in the localStorage
  localStorage.setItem(STORAGE_KEY + "-fencepostdistance",customMaxDistance);
  // Making the reset to default button enabled
  document.getElementById("resetButton").removeAttribute("disabled");
}

 //Function which runs when the user wants to reset the MaxDistance back to default 
 function resetDistance()
{
   // removing the customMaxDistance stored in localStorage by the user
   localStorage.removeItem(STORAGE_KEY + "-fencepostdistance");
   // Displaying the currentMaxDistance as default Max Distance
   document.getElementById("currentMaxDistance").innerText = defaultMaxDistance + " metres";
   // Disabling the reset to default button
   document.getElementById("resetButton").setAttribute("disabled","disabled")  
}
