// Code for the View Region page.

// Storing Regions in an Array
var regionsArray = JSON.parse(localStorage.getItem("monash.eng1003.fencingApp"));

//  Region Index stores the Index of Region viewed by the user in the main page
var regionIndex = localStorage.getItem(STORAGE_KEY + "-selectedRegion"); 

// Drawing the map centered on the user's registered Location
 mapboxgl.accessToken = 'pk.eyJ1IjoibW9iaWxlYXBwc3RlYW0zIiwiYSI6ImNraTRuNjJoYTI4Y2wyeW5tNnNxajYzbzAifQ.4utSTWmbPKUJKhiXnGl83A';
             
            
 let map = new mapboxgl.Map({
     container: 'map',
     style: 'mapbox://styles/mapbox/streets-v10',
     zoom: 17,
     // Viewed Region centers to the point registered by the user
     center: [regionsArray[regionIndex]._registeredLocation[0].lng, regionsArray[regionIndex]._registeredLocation[0].lat]
    });


// Displaying the name of the region clicked by the user on the header 
if (regionIndex !== null)
{

    var regionNames = [];
    
    for (i = 0;i < regionsArray.length;i++)
    {
      regionNames.push(regionsArray[i]._nickname)      
    }
    
    // If a region index was specified, show name in header bar title
    document.getElementById("headerBarTitle").textContent = regionNames[regionIndex];
}


// Code below is to run when user views the one of the regions he has stored
{
    
    // To display marker on the registered point
    /*var marker = new mapboxgl.Marker()
      .setLngLat(regionsArray[regionIndex]._registeredLocation[0])
      .addTo(map);*/
    
    // Drawing the polygon by using cornerlocations coordinates of the region viewed stored in the regionsArray
    map.on('load', function () {
           map.addSource('maine', {
          'type': 'geojson',
          'data': {
          'type': 'Feature',
          'geometry': {
          'type': 'Polygon',
          'coordinates': [regionsArray[regionIndex]._cornerLocations[0]]
          
          }
        }
                 
     });
                  
          map.addLayer({
         'id': 'maine',
         'type': 'fill',
         'source': 'maine',
         'layout': {},
         'paint': {
         'fill-color': '#088',
         'fill-opacity': 0.8     
         }       
      });        
    });
}


// Function which runs when the user wants to delete the region
function DeleteRegion()
{
    // Confirming whether the user wants to delete the region
    if (confirm('Are you sure you want delete this Region ')) {
    
    // Storing all the regions in local Storage in an array
    var Regions = JSON.parse(localStorage.getItem("monash.eng1003.fencingApp"));

    // Storing Index of the region which is to be deleted
    var indexToRemove = regionIndex;

    // Removing the region to be deleted from the array 
    Regions.splice(indexToRemove, 1);
    
    // Storing back all the regions to local Storage without the delelted Region
    localStorage.setItem(STORAGE_KEY,JSON.stringify(Regions));
          
    window.location.href = "index.html";
    } 
    else 
    {
     
      console.log('No region deleted');
    }  
}

// Function which runs when the user wants to calculate the area of the region
function Area()

{
  // Creating Region class Instances
  var regionArea = new Region(regionsArray[regionIndex]._nickname,new Date().toLocaleDateString(),new Date().toLocaleTimeString(),regionsArray[regionIndex]._registeredLocation[0]);
    
  // Calling function in region Class to calculate Area of Region
  regionArea.calculateArea(regionsArray[regionIndex]._cornerLocations[0]);
}


// Function which runs when the user wants to calculate the perimeter of the region
function Perimeter()
{
   // Creating Region class Instances
   var regionPerimeter = new Region(regionsArray[regionIndex]._nickname,new Date().toLocaleDateString(),new Date().toLocaleTimeString(),regionsArray[regionIndex]._registeredLocation[0]);
  
  // Calling function in region Class to calculate perimeter of Region
  regionPerimeter.calculatePerimeter(regionsArray[regionIndex]._cornerLocations[0]);
    
}

// Function which runs when the user Toggles on and off the Fence Posts

function Fence(checkbox)
{
    // Storing the corner Locations of the polygon in an array
    var cornerLocations  = regionsArray[regionIndex]._cornerLocations[0];

    var currentMarkers=[];
    
    // if checked starting the process of displaying the fence posts on the map
    if (checkbox.checked == true)
    {
        // Creating an instance of the Region Class so that its fence method can be accesible
        var regionFence = new Region(regionsArray[regionIndex]._nickname,new Date().toLocaleDateString(),new
    Date().toLocaleTimeString(),regionsArray[regionIndex]._registeredLocation[0]);
    
      // Storing the returned fenceLocations from shared.js(by the calling the fence() method in region Class )in an array
      var fenceLocations =  regionFence.fence(cornerLocations);
        
      

      // For loop to plot the fence posts as markers around the polygon
      for(i = 0;i < fenceLocations.length;i++)
          {
            
            // Object to store the Lng and Lat values of the fence Location
            var coordinateObject = 
             {
                lng : fenceLocations[i][0],
                lat : fenceLocations[i][1]
            };
           
            // Displaying the marker based on the fence Location 
            {
               var marker = new mapboxgl.Marker()
               .setLngLat(coordinateObject)
               .addTo(map)
               
             }
   
          }
        
       // For loop to plot the corner Locations as markers
       for(j = 0; j < cornerLocations.length;j++)
           {
             var coordinateObject = 
             {
                lng : cornerLocations[j][0],
                lat : cornerLocations[j][1]
            };
           
              
             {
               var marker = new mapboxgl.Marker()
               .setLngLat(coordinateObject)
               .addTo(map)
               
               
             }  
           }
        
        
   }
    // Code which runs when the toggle is off
    else   
    {
        
        
    }
}
        
    
    
       

    


// Function to show a DialogBox of the Area and Perimeter calculated of a region
function showDialog(){
  let dialog = document.querySelector('dialog');
  if (!dialog.showModal)
  {
    dialogPolyfill.registerDialog(dialog);
  }
  dialog.showModal();
  dialog.querySelector('.close').addEventListener('click', function() {
    dialog.close();
  });
}
