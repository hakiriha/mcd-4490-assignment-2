// Code for the Record Region page.

var coordinatesArray,nickname;
var initialLocation  = [];

 //Function which runs when the save region button is clicked
 function saveRegion()
 {
     //Checking if there are any Regions already saved in local Storage
     if (STORAGE_KEY in localStorage)
     {
        savedRegions = JSON.parse(localStorage.getItem(STORAGE_KEY)); 
     } 
   
     
    var data = draw.getAll(); // Collecting Information of the polygon drawn
     
    if (data.features.length === 0)
        {
            alert('Region should have more than 3 corners');
            
        }
    else
        {
            // Storing the coordinates of the corners of the polygon in an array
            coordinatesArray = data.features[0].geometry.coordinates[0];
             
             
            //Asking the user for a nickname to name the Region
            nickname = Nickname()
            if (nickname === null)
            {
                return;
            }
             
            //Creating an instance of the Region class and setting Date,Time, nickname and registered Location
            var newRegion = new Region(nickname,new Date().toLocaleDateString(),new Date().toLocaleTimeString(),initialLocation);
            //adding the cornerlocations to the Region created
            newRegion.addlocation(coordinatesArray);
    
            //Creating an instance of the RegionList class and pushing Regions to the RegionsList
            var newRegionList = new RegionList();
            newRegionList.addregion(newRegion);
            
            //Adding the available Regions along with the new Region saved to the local Storage
            localStorage.setItem(STORAGE_KEY,JSON.stringify(savedRegions));
             
            window.location.href = "index.html";
         }
    
 }


 //Function which runs when the register button is clicked         
 function registerLocation()
 {
    document.getElementById("RegisterButton").onclick = null;
    document.getElementById("SaveButton").removeAttribute('disabled');
    
    // Code to obtain coordinates of the registered Location and to display registered Location as a marker
    map.once('click', function(e)
   {
     // map flies to the user's registered Location
     map.flyTo({center:e.lngLat}) //map centers to the location picked by the user
     
     var popup = new mapboxgl.Popup({ offset: 25 }).setText(e.lngLat);
     
        
     var marker1 = new mapboxgl.Marker()
     .setLngLat(e.lngLat)
     .setPopup(popup)
     .addTo(map);

     // Array to store the coordinates of the user's registered Location    
     initialLocation.push(e.lngLat);
      console.log(initialLocation)
        
     // Displaying the coordinates of the registered location to the user
      message = "Your Location " + String(e.lngLat);
     
      displayMessage(message, 2)
     
    });  
      
  }  

// Function to validate nickname of Region saved by user
function Nickname()
{
   nickname = prompt('Enter a nickname');
             
   if (nickname == null)
  {
    return null
  }
   else if (nickname == "")
  {
    Nickname();
  }
    
  return nickname
     
}